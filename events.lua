--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ADDON VARIABLES
--------------------------------------------------------------------------------------------------------------------------------------------------------------
print("|cff00CCFFevents.lua loaded!")
local name, addon = ...
local Unit = addon.Unit
local player = addon.player
local target = addon.target
local Convert_To_Percent = addon.Convert_To_Percent
local Readable_Number = addon.Readable_Number
local Set_Stat_Text = addon.Set_Stat_Text
local colour = addon.colour
local Colour = addon.Colour
local printf = addon.printf
local default = addon.default
local casting = addon.casting
local texture = addon.texture

local function Update_Unit(self,unit)
    -- Update variables
    self.name                               = UnitName(unit)
    self.class.type                         = select(2,UnitClass(unit))
    self.class.colour.r                     = RAID_CLASS_COLORS[self.class.type].r
    self.class.colour.g                     = RAID_CLASS_COLORS[self.class.type].g
    self.class.colour.b                     = RAID_CLASS_COLORS[self.class.type].b
    
    self.health.current                     = UnitHealth(unit)
    self.health.max                         = UnitHealthMax(unit)
    self.health.percent                     = Convert_To_Percent(self.health)

    self.power.current                      = UnitPower(unit)
    self.power.max                          = UnitPowerMax(unit)
    self.power.percent                      = Convert_To_Percent(self.power)
    self.power.type                         = select(2,UnitPowerType(unit))
        
    -- Assign level
    if UnitLevel(unit) >= 1 then
        self.level = UnitLevel(unit)
    else 
        self.level = "??" 
    end
    
    -- Assign classification
    local UnitClassification = UnitClassification(unit)
    local classification
    
    if UnitClassification == "trivial" then
        classification = "NO EXP Level "
    elseif UnitClassification == "normal" then
        classification = "Level "
    elseif UnitClassification == "rare" then
        classification = "Rare Level "
    elseif UnitClassification == "elite" then
        classification = "|cffFFCC33Elite Level "
    elseif UnitClassification == "rareelite" then
        classification = "Rare Elite Level "
    elseif UnitClassification == "worldboss" then
        classification = "Boss Level "
    else
        classification = "Level "
    end
    
    -- Colour text based on power type
    local power = {}
    if self.power.type == "MANA" then
        power.colour = colour.MANA 
    elseif self.power.type == "RAGE" then
        power.colour = colour.RAGE 
    elseif self.power.type == "FOCUS" then
        power.colour = colour.FOCUS 
    elseif self.power.type == "ENERGY" then
        power.colour = colour.ENERGY 
    elseif self.power.type == "CHI" then
        power.colour = colour.CHI 
    elseif self.power.type == "RUNIC_POWER" then
        power.colour = colour.RUNIC_POWER
    elseif self.power.type == "POWER_TYPE_FEL_ENERGY" then
        power.colour = colour.FEL
    else
        power.colour = colour.WHITE
    end
    
    -- Update texts/bars
    self.text.name:SetText(self.name)
    self.text.name:SetTextColor(self.class.colour.r,self.class.colour.g,self.class.colour.b) 
    self.text.level:SetText(classification .. self.level)
    self.text.power:SetTextColor(Colour(power.colour))
    
    --self.castbar.frame:SetStatusBarColor(Colour(power.colour)) -- class Colour castbar
    self.healthbar.frame:SetValue(self.health.percent)
    self.powerbar.frame:SetStatusBarColor(self.class.colour.r,self.class.colour.g,self.class.colour.b)
    
    -- Show percent if more than 100%
    Set_Stat_Text(self,"health")
    Set_Stat_Text(self,"power")

    self.healthbar.frame:Show()
    self.powerbar.frame:Show()
end


--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EVENTS
--------------------------------------------------------------------------------------------------------------------------------------------------------------
local handler = CreateFrame("Frame")						                    -- Event handler
local event = {}										                        -- A table of events to store the events I will use (FYI, var.string is the same as var["string"] )

-- Player Login
function event.PLAYER_LOGIN(...)
    target.healthbar.frame:Hide()
    target.powerbar.frame:Hide()
    target.castbar.frame:Hide()
    player.castbar.frame:Hide()
    Update_Unit(player,"player") -- Update unit info/frame
end

-- Selected/deselected Target
function event.PLAYER_TARGET_CHANGED(...)
    -- Target Exists
    if UnitExists("target") then
        target.castbar.frame:Hide()
        Update_Unit(target,"target") -- Update unit info/frame
    else
        target.healthbar.frame:Hide()
        target.powerbar.frame:Hide()
    end
end

-- Level Up
function event.PLAYER_LEVEL_UP(...)
    Update_Unit(player,"player")
end

-- Unit Health Max
function event.UNIT_MAXHEALTH(...)
    if ... == "player" then
    player.health.max = UnitHealthMax("player")			                       -- Update current max health -- TODO: Check for out of combat health changes
    end
end

-- Unit Power Max
function event.UNIT_MAXPOWER(...)
    if ... == "player" then
        player.power.max = UnitPowerMax("player")		                       -- Update current max health -- TODO: Check for out of combat health changes
    end
end

-- Unit Health
function event.UNIT_HEALTH(...)
    if ... == "player" then
        player.health.current = UnitHealth("player")				           -- Update current health
        player.health.percent = addon.Convert_To_Percent(player.health)        -- Update health percent
        
        Set_Stat_Text(player,"health")                                         -- update health text
        player.healthbar.frame:SetValue(player.health.percent)                 -- Update health bar
    end
    
    if ... == "target" then
        target.health.current = UnitHealth("target")				           -- Update current health
        target.health.percent = addon.Convert_To_Percent(target.health)        -- Update health percent
       
        Set_Stat_Text(target,"health")                                         -- update health text
        target.healthbar.frame:SetValue(target.health.percent)                 -- Update health bar
    end
end

-- Unit Power
function event.UNIT_POWER(...)
    if ... == "player" then
        player.power.current = UnitPower("player")				               -- Update current health
        player.power.percent = addon.Convert_To_Percent(player.power)          -- Update health percent
        Set_Stat_Text(player,"power") 
        player.powerbar.frame:SetValue(player.power.percent)                   -- Represent health via health bar texture
    end
    
    if ... == "target" then
        target.power.current = UnitPower("target")				               -- Update current power
        target.power.percent = addon.Convert_To_Percent(target.power)          -- Update power percent
        Set_Stat_Text(target,"power") 
        target.powerbar.frame:SetValue(target.health.percent)                  -- Update power bar
        
    end
end

-- Level Up
function event.PLAYER_LEVEL_UP(...)
    local level = ...
    player.level = tonumber(level)
    -- Assign level
    player.text.level:SetText("Level " .. player.level)
end


local timer = 0

-- unit casting function
function Unit_Casting(self,unit)
    self.castbar.frame:Show()

    local spell, rank, displayName, icon, startTime, endTime, isTradeSkill, castID, interrupt = UnitCastingInfo(unit)
    self.spell.casting = true
    self.spell.name = spell
    self.spell.time.current = 0
    self.spell.time.total = (endTime - startTime) / 1000
    self.text.spell.time.current:SetText(string.format ("%.1f",self.spell.time.current))
    self.text.spell.time.total:SetText("") -- empty string
    self.text.spell.name:SetText(self.spell.name)
end

--unit stopped casting function
function Unit_Stopped_Casting(self,unit)
    self.castbar.frame:Hide()

    self.spell.casting = false
    self.text.spell.time.current:SetText("TIME")
    self.text.spell.name:SetText("SPELL NAME")
    self.castbar.frame:SetValue(0)
    self.spell.timer = 0
    self.spell.time.current = 0
end

-- Casting Started
function event.UNIT_SPELLCAST_START(...)
    if ... == "player" then
        Unit_Casting(player,"player")
    elseif ... == "target" then
        Unit_Casting(target,"target")
    end
end

-- Casting Stopped
function event.UNIT_SPELLCAST_STOP(...)
    if ... == "player" then
        Unit_Stopped_Casting(player,"player")
    elseif ... == "target" then
        Unit_Stopped_Casting(target,"target")
    end
end

-- Loop to register events above
for key, value in pairs(event) do
 handler:RegisterEvent(key)                                                    -- Register all events for which handlers have been defined
    --print("Event Registered: " .. key)
end

-- Run a function above when an event triggers
local function Process_Event(self,event_triggered,arg1,...)
	event[event_triggered](arg1)
	--print("Event Triggered: " .. event_triggered, arg1)
end

-- Listen for a triggered registered event
handler:SetScript("OnEvent", Process_Event)
