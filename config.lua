--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- CONFIG FILE
--------------------------------------------------------------------------------------------------------------------------------------------------------------
print("|cff00CCFFconfig.lua loaded!")
local name, addon = ...

addon.options = 
{
    player = 
    {
        frame        = true,
        castbar      = true,
        moveable     = false,
        position_x   = -150,
        position_y   = 100,
        scale        = 1
    },

    target = 
    {
        frame        = true,
        castbar      = true,
        moveable     = false,
        position_x   = 150,
        position_y   = 100,
        scale        = 1
    }
}

