--//TODO: Toggle movability via right click menu
--//TODO: Level Difficulty colouring
--//TODO: Make a config window
--//FIX: Fix player frame update for druid shapeshift

--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ADDON VARIABLES
--------------------------------------------------------------------------------------------------------------------------------------------------------------
print("|cff00CCFFcore.lua loaded!")
local name, addon = ...

--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- MEDIA
--------------------------------------------------------------------------------------------------------------------------------------------------------------
local texture = 
{
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Angelique",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Antonia",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Bettina",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Jasmin",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Larissa",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Lisa",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Sam",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Stella",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\BantoBar",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\textures\\Grad"
}

local font = 
{
    "Fonts\\FRIZQT__.TTF",
    "Interface\\AddOns\\MyUI_UnitFrames\\media\\fonts\\Elronmonospace.TTF"
}

--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- COLOURS (RGB FORMAT)
--------------------------------------------------------------------------------------------------------------------------------------------------------------
local colour = 
{
    WHITE       = {255,255,255},
    SPACEBLACK  = {65,74,76},
    
    RED         = {247,111,111},
    DARKRED     = {147,11,11},
    
    GREEN       = {168,219,168},
    DARKGREEN   = {100,141,100},
    
    BLUE        = {16,127,201},
    DARKBLUE    = {0,81,155},
    
    GOLD        = {212,175,55},
    
    MANA        = {16,127,201},
    RAGE        = {255,0,0},
    FOCUS       = {255,128,64},
    ENERGY      = {255,255,0},
    CHI         = {181,255,235},
    RUNES       = {128,128,128},
    RUNIC_POWER = {0,209,255},
    FEL         = {186,231,13}
}

--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FUNCTIONS
--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Converts RGB values to compatible values
local function Colour(colour,alpha)
	return colour[1]/255, colour[2]/255, colour[3]/255, alpha
end

-- Converts to a percentage and returns the value
local function Convert_To_Percent(value)
    local percent

        -- prevents: -1%
        if value.current > 0 and value.max > 0 then
            percent = (value.current/value.max) * 100
        else
            percent = 0
        end

    return percent
end

-- Converts a large number to readable format
local function Readable_Number(num, places)
    local ret
    local placeValue = ("%%.%df"):format(places or 1)
    
        if not num then
            return 0
        elseif num >= 1e12 then
            ret = placeValue:format(num / 1e12) .. " Tril"  -- trillion
        elseif num >= 1e9 then
            ret = placeValue:format(num / 1e9) .. " Bil"    -- billion
        elseif num >= 1e6 then
            ret = placeValue:format(num / 1e6) .. " Mil"    -- million
        elseif num >= 1e3 then
            ret = placeValue:format(num / 1e3) .. "k"       -- thousand
        else
            ret = num                                       -- hundreds
        end

    return ret
end

-- Display Text Function
local function Set_Stat_Text(unit, stat)

    -- When unit is less than 100% stat, show percent
    if unit[stat].percent >= 1 and unit[stat].percent <= 99 then
        unit.text[stat]:SetText(Readable_Number(unit[stat].current) .. " - " .. string.format("%0.0f",unit[stat].percent) .. "%")
    else 
    -- When unit is at 100% stat, hide percent
        unit.text[stat]:SetText(Readable_Number(unit[stat].current))
    end 
    
    -- When unit is dead
    if unit.health.current < 1 then
        unit.text[stat]:SetText("|cff000000DEAD")
    end
end

-- Print in colour
local function printf(...)
    print("|cff00CCFF",...)
end

--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- DEFAULTS
--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Default
local default =
{
    width                   = 220 * addon.options.player.scale,
    height                  = 35 * addon.options.player.scale,
    texture                 = texture[2],
    statusbar_colour        = colour.SPACEBLACK,
    font                    = {type = font[1], size = 10 * addon.options.player.scale}
}

local casting = false

--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- UNIT CLASS
--------------------------------------------------------------------------------------------------------------------------------------------------------------
Unit = {}
function Unit:New(unit)    
    
    -- Declarations
    local self = 
    {
        name,
        level,
        health      = { current, max, percent },
        power       = { current, max, percent, type },
        class       = { type, colour = {r,g,b} },
        spell       = { name, casting, timer, time = {current, total} },
        frame,
        healthbar   = { frame, background, border, texture, height ,width },
        powerbar    = { frame, background, border, texture, height ,width },
        castbar     = { frame, background, border, texture, height ,width },
        text        = { name, level, health, power, spell = { name, time = { total, current} }, font = {type, size} }
    }
    
    -- HEALTH BAR --------------------------------------------------------------------------------------
    -- Main Layer
    self.healthbar.frame = CreateFrame("StatusBar","MyUI_"..tostring(unit).."_healthframe",UIParent,"SecureActionButtonTemplate")       -- Create frame
    self.healthbar.frame:SetStatusBarTexture(default.texture)	                                        -- Set statusbar texture (texture,layer)
    
    self.healthbar.frame:SetPoint("CENTER", UIParent, "CENTER", addon.options[unit].position_x, addon.options[unit].position_y) --set unit frame position

    self.healthbar.frame:SetSize(default.width, default.height)		                                    -- Set width and height
    self.healthbar.frame:SetStatusBarColor(Colour(default.statusbar_colour))							-- Set colour
    self.healthbar.frame:SetMinMaxValues(0, 100)											            -- Set the minimum and maximum values of the status bar
    self.healthbar.frame:SetFrameStrata("BACKGROUND")

    -- Background Layer
    self.healthbar.background = self.healthbar.frame:CreateTexture(nil, "BACKGROUND")                   -- Create texture
    self.healthbar.background:SetTexture(default.texture)	                                            -- Set statusbar background texture
    self.healthbar.background:SetAllPoints(true)                                                        -- Set region points to match of main layer
    self.healthbar.background:SetVertexColor(Colour(colour.DARKRED))                                    -- Set colour for background layer
    self.healthbar.background:SetDrawLayer("BACKGROUND")

    -- Border
    self.healthbar.border = CreateFrame("Frame", "MyUI_"..tostring(unit).."_healthborder", self.healthbar.frame)                             -- Create frame
    self.healthbar.border:SetSize(default.width, default.height)                                        -- Size of border frame
    self.healthbar.border:SetPoint("CENTER")                                                            -- Alignment of border frame
    self.healthbar.border:SetBackdrop({edgeFile="Interface\\ChatFrame\\ChatFrameBackground",edgeSize=1})-- Border texture and size
    self.healthbar.border:SetBackdropBorderColor(0,0,0)                                                 -- Border colour

    -- POWER BAR ---------------------------------------------------------------------------------------
    -- Main Layer
    self.powerbar.frame = CreateFrame("StatusBar", "MyUI_"..tostring(unit).."_powerframe", UIParent)                                       -- Create frame
    self.powerbar.frame:SetStatusBarTexture(default.texture)	                                        -- Set statusbar texture
    self.powerbar.frame:SetPoint("TOP", self.healthbar.frame, "BOTTOM", 0, 1)		                    -- Set position
    self.powerbar.frame:SetSize(default.width, default.height/7)		                                -- Set width and height
    self.powerbar.frame:SetStatusBarColor(Colour(default.statusbar_colour))	                            -- Set colour
    self.powerbar.frame:SetMinMaxValues(0, 100)											                -- Set the minimum and maximum values of the status bar

    -- Background Layer
    self.powerbar.background = self.powerbar.frame:CreateTexture(nil, "BACKGROUND")                     -- Create texture
    self.powerbar.background:SetTexture(default.texture)	                                            -- Set statusbar background texture
    self.powerbar.background:SetAllPoints(true)                                                         -- Set region points to match of top layer
    self.powerbar.background:SetVertexColor(0.2,0.2,0.2)                                                -- Set colour for background layer

    -- Border
    self.powerbar.border = CreateFrame("Frame", "MyUI_"..tostring(unit).."_powerborder", self.powerbar.frame)                               -- Create frame
    self.powerbar.border:SetSize(default.width,default.height/7)                                        -- Size of border frame
    self.powerbar.border:SetPoint("CENTER")                                                             -- Alignment of border frame
    self.powerbar.border:SetBackdrop({edgeFile="Interface\\ChatFrame\\ChatFrameBackground",edgeSize=1}) -- Border texture and size
    self.powerbar.border:SetBackdropBorderColor(0,0,0)                                                  -- Border colour

    -- TEXT --------------------------------------------------------------------------------------------
    -- Name
    self.text.name = self.healthbar.frame:CreateFontString(nil, "OVERLAY")                              -- Create font string
    self.text.name:SetFont(default.font.type, default.font.size, nil)                                   -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    self.text.name:SetPoint("TOPLEFT", self.healthbar.frame, "TOPLEFT", 5, -5)                          -- Text location
    self.text.name:SetJustifyH("LEFT")                                                                  -- Text justification
    self.text.name:SetShadowOffset(1, -1)                                                               -- Text shadow
    self.text.name:SetTextColor(1,1,1)                                                                  -- Text colour
    self.text.name:SetText("NAME")                                                                      -- Name text
    -- Level
    self.text.level = self.healthbar.frame:CreateFontString(nil, "OVERLAY")                             -- Create font string
    self.text.level:SetFont(default.font.type, default.font.size, nil)                                  -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    self.text.level:SetPoint("TOPRIGHT", self.healthbar.frame, "TOPRIGHT", -5, -5)                      -- Text location
    self.text.level:SetJustifyH("RIGHT")                                                                -- Text justification
    self.text.level:SetShadowOffset(1, -1)                                                              -- Text shadow
    self.text.level:SetTextColor(1,1,1)                                                                 -- Text colour
    self.text.level:SetText("LEVEL")                                                                    -- Level text
    -- Health Text
    self.text.health = self.healthbar.frame:CreateFontString(nil, "OVERLAY")                            -- Create font string
    self.text.health:SetFont(default.font.type, default.font.size, nil)                                 -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    self.text.health:SetPoint("BOTTOMRIGHT", self.healthbar.frame, "BOTTOMRIGHT", -5, 5)                -- Text location
    self.text.health:SetJustifyH("RIGHT")                                                               -- Text justification
    self.text.health:SetShadowOffset(1, -1)                                                             -- Text shadow
    self.text.health:SetTextColor(Colour(colour.GREEN))                                                 -- Text colour
    self.text.health:SetText("HEALTH")                                                                  -- Health text
    -- Power Text
    self.text.power = self.healthbar.frame:CreateFontString(nil, "OVERLAY")                             -- Create font string
    self.text.power:SetFont(default.font.type, default.font.size, nil)                                  -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    self.text.power:SetPoint("BOTTOMLEFT", self.healthbar.frame, "BOTTOMLEFT", 5, 5)                    -- Text location
    self.text.power:SetJustifyH("LEFT")                                                                 -- Text justification
    self.text.power:SetShadowOffset(1, -1)                                                              -- Text shadow
    self.text.power:SetTextColor(1,1,1)                                                                 -- Text colour
    self.text.power:SetText("POWER")                                                                    -- Power text

    -- -- CAST BAR --------------------------------------------------------------------------------------------------------------------------------------------------
    -- -- Main Layer
    -- self.castbar.frame = CreateFrame("StatusBar", "MyUI_"..tostring(unit).."_castbar", UIParent)        -- Create frame
    -- self.castbar.frame:SetStatusBarTexture(texture[9])	                                            -- Set statusbar texture
    -- self.castbar.frame:SetPoint("TOP", self.powerbar.frame, "BOTTOM", 0, -5)		                    -- Set position
    -- self.castbar.frame:SetSize(default.width, default.height/2)		                                    -- Set width and height
    -- self.castbar.frame:SetStatusBarColor(1,1,1)	                                                        -- Set colour
    -- self.castbar.frame:SetMinMaxValues(0,100)											                -- Set the minimum and maximum values of the status bar
    -- self.castbar.frame:SetValue(0)                                                                      -- Set default value

    -- -- Background Layer
    -- self.castbar.background = self.castbar.frame:CreateTexture(nil, "BACKGROUND")                       -- Create texture
    -- self.castbar.background:SetTexture(texture[9])	                                                -- Set statusbar background texture
    -- self.castbar.background:SetAllPoints(true)                                                          -- Set region points to match of top layer
    -- self.castbar.background:SetVertexColor(0.2,0.2,0.2)                                                 -- Set colour for background layer

    -- -- Border
    -- self.castbar.border = CreateFrame("Frame", nil, self.castbar.frame)                                 -- Create frame
    -- self.castbar.border:SetSize(default.width,default.height/2)                                         -- Size of border frame
    -- self.castbar.border:SetPoint("CENTER")                                                              -- Alignment of border frame
    -- self.castbar.border:SetBackdrop({edgeFile="Interface\\ChatFrame\\ChatFrameBackground",edgeSize=1})  -- Border texture and size
    -- self.castbar.border:SetBackdropBorderColor(0,0,0)                                                   -- Border colour

    -- -- Spell Time Current Text
    -- self.text.spell.time.current = self.castbar.frame:CreateFontString(nil, "OVERLAY")                  -- Create font string
    -- self.text.spell.time.current:SetFont(default.font.type, default.font.size, nil)                     -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    -- self.text.spell.time.current:SetPoint("RIGHT", self.castbar.frame, "RIGHT", -5, 1)                  -- Text location
    -- self.text.spell.time.current:SetJustifyH("RIGHT")                                                   -- Text justification
    -- self.text.spell.time.current:SetShadowOffset(1, -1)                                                 -- Text shadow
    -- self.text.spell.time.current:SetTextColor(1,1,1)                                                    -- Text colour
    -- self.text.spell.time.current:SetText("TIME")                                                        -- Current spell casting time

    -- -- Spell Name Text
    -- self.text.spell.name = self.castbar.frame:CreateFontString(nil, "OVERLAY")                          -- Create font string
    -- self.text.spell.name:SetFont(default.font.type, default.font.size, nil)                             -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    -- self.text.spell.name:SetPoint("CENTER", self.castbar.frame, "CENTER", 0, 1)                         -- Text location
    -- self.text.spell.name:SetJustifyH("MIDDLE")                                                            -- Text justification
    -- self.text.spell.name:SetShadowOffset(1, -1)                                                         -- Text shadow
    -- self.text.spell.name:SetTextColor(1,1,1)                                                            -- Text colour
    -- self.text.spell.name:SetText("SPELL NAME")                                                          -- Spell Name

    -- -- Spell Time Total Text
    -- self.text.spell.time.total = self.castbar.frame:CreateFontString(nil, "OVERLAY")                  -- Create font string
    -- self.text.spell.time.total:SetFont(default.font.type, default.font.size, nil)                     -- Font, size and flags (MONOCHROME, OUTLINE, THICKOUTLINE)
    -- self.text.spell.time.total:SetPoint("LEFT", self.castbar.frame, "LEFT", 5, 1)                  -- Text location
    -- self.text.spell.time.total:SetJustifyH("LEFT")                                                   -- Text justification
    -- self.text.spell.time.total:SetShadowOffset(1, -1)                                                 -- Text shadow
    -- self.text.spell.time.total:SetTextColor(1,1,1)                                                    -- Text colour
    -- self.text.spell.time.total:SetText("TOTAL")                                                        -- Current spell casting time

    -- Enable Moveability
    if addon.options[unit].moveable == true then
    self.healthbar.frame:SetMovable(true)
    self.healthbar.frame:EnableMouse(true)
    self.healthbar.frame:RegisterForDrag("LeftButton")
    self.healthbar.frame:SetScript("OnDragStart", self.healthbar.frame.StartMoving)
    self.healthbar.frame:SetScript("OnDragStop", self.healthbar.frame.StopMovingOrSizing)
    end
    
    -- Click functionality (select self, right-click menu hitbox)
    self.frame = CreateFrame("BUTTON","MyUI_"..tostring(unit).."_selectframe", self.healthbar.frame, "SecureActionButtonTemplate")
    self.frame:SetPoint("CENTER",0,0)
    self.frame:SetSize(default.width-6, default.height-6)
    self.frame:EnableMouse(true)
    self.frame:RegisterForClicks("AnyUp")
    --self.frame:SetNormalTexture(default.texture) -- for debugging

    -- menu options depending on target
    local menu
    if unit == "player" then
        self.frame:SetAttribute("type1", "macro")
        self.frame:SetAttribute("macrotext", "/target player")
        menu = PlayerFrameDropDown
    elseif unit == "target" then
        menu =TargetFrameDropDown
    end

    --enable right-click menu
    SecureUnitButton_OnLoad(self.frame, unit, function()
    ToggleDropDownMenu(1, nil, menu, "cursor")
    end)

    print("created:" .. unit)
    return self
end

-- Create Units
addon.player = Unit:New("player")
addon.target = Unit:New("target")

-- Add frames to UISpecialFrames (to hide targeframe when selecting player)
--table.insert(UISpecialFrames, "MyUI_target_healthframe")
--table.insert(UISpecialFrames, "MyUI_target_powerframe")

-- Add to addon variable
addon.Unit = Unit
addon.Colour = Colour
addon.default = default
addon.texture = texture
addon.font = font
addon.colour = colour
addon.casting = casting
addon.Convert_To_Percent = Convert_To_Percent
addon.Readable_Number = Readable_Number
addon.Set_Stat_Text = Set_Stat_Text
addon.printf = printf

