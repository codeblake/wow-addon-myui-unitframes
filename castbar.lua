--//TODO: Channeling castbar
--//TODO: Interrupt/pushback casting update
--//FIX: When selecting target after casting, show correct castbar time
--------------------------------------------------------------------------------------------------------------------------------------------------------------
-- ADDON VARIABLES
--------------------------------------------------------------------------------------------------------------------------------------------------------------
print("|cff00CCFFcastbar.lua loaded!")
local name, addon = ...

local player = addon.player
local target = addon.target

local handler = CreateFrame("Frame")				        -- Event handler
local event = {}									        -- A table of events to store the events I will use (FYI, var.string is the same as var["string"] )
local timer = 0

-- CASTBAR ANIMATION EVENTS ----------------------------------------------------------------------------------------------------------------------------------  
function Update_Castbar(self,elapsed)
    if not self.spell.timer then self.spell.timer = 0 end   -- create timer

    self.spell.timer = self.spell.timer + elapsed           -- update timer

    if self.spell.timer < 0 then self.spell.timer = 0 end   -- prevents negative numbers
    
    --Update castbar values
    self.castbar.frame:SetMinMaxValues(0,self.spell.time.total)
    self.castbar.frame:SetValue(self.spell.timer)

    -- Update castbar text
    self.text.spell.time.current:SetText(string.format("%.1f",self.spell.timer))
end

-- On Update
local function onUpdate(self,elapsed)
    if player.spell.casting == true then
        Update_Castbar(player,elapsed)
    end

    if target.spell.casting == true then
        Update_Castbar(target,elapsed)
    end
end

local f = CreateFrame("frame")
f:SetScript("OnUpdate", onUpdate)