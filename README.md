# MyUI_UnitFrames#
A WIP World of Warcraft UI addon

### Addon Goals###
* Memory efficiency
* Balanced features
* Replace all UnitFrames from the UI

### Current Features ###
* Player and Target Frames
* Class colours names and energy bar
* Level classification (rare,elite,etc)
* Health and energy text - including %
* Right click functionality
* Basic Player cast bar